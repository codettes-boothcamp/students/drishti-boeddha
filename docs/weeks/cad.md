***TINKERCAD***

open tinkercad-> got to 3d designs-. open new design(or existing if possible)
click on the object u want and drag it to the workplate

![1](../img/cad1.png)

to add a hole click on the obect-> click hole

![2](../img/cad2.png)

the result of a hole *note: u can also place a hole in a solid object*

![3](../img/cad3.png)

*ctrl+s to save* and export after saving to 3d print

![4](../img/cad4.png)

choose ur 3d material which u wanna use to print

![5](../img/cad5.png)

the final object:

![6](../img/final.png)




***FUSION 360***

open google-> search the object's base that u wanna add and save that image as png/jpg

![1](../img/fu1.png)

open inkscape and import ur image

![2](../img/fu2.png)

go to paths-> trace bitmap (to make a replica of ur image so that u can delete ur image so the replica can be saved to 3d print)

![3](../img/fu3.png)

adjust the color and details of ur replica

![4](../img/fu4.png)

the result: and delete ur img(red)

![5](../img/fu5.png)

*note: save ur replica as SVG file*

open fusion 360->insert->SVG

![6](../img/fu6.png)

![7](../img/fu7.png)

when ur file is opened, click E on the object(extrude) and extrude the object ur size

![8](../img/fu8.png)

click ctrl+S->export

![9](../img/fu9.png)

