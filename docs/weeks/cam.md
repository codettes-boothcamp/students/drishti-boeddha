***OpenScad***


Openscad is a software to create 3d parametric designs which can later be designed into product models
download open OpenScad

**we will create a speaker box*

first, create the object using the params etc

![1](../img/cadmd/cadmd1.png)

after creating the parametric design, click on "file->export->svg"

![2](../img/cadmd/cadmd2.png)

open Makercam.com (it is not in use anymore but u can still use it) ->enable flash-> inspect element(right click)->html element:body->content->container->Change CSS “height:100%” to “height:100vh”

![3](../img/cadmd/cadmd3.png)

file->open svg

![4](../img/cadmd/cadmd4.png)

put the following parameters for the objects: by clicking on "file->pocket operation and profile operation"

![p1](../img/cadmd/params1.png)

![p2](../img/cadmd/params2.png)

![3](../img/cadmd/params3.png)

after getting all the parameters, go to "toolpath" to make sure that all of the parts have the parameters
then u go to "cam-> calculate all" to check for any errors

![5](../img/cadmd7.png)

if everything is okay, go to "cam-> export G code"

![6](../img/cadmd8.png)

click on "all, standard G code and then export selected toolpaths"


