***KICAD***

install through this link: https://kicad-pcb.org/download   

Make a new file

![1](../img/ed/ed1.png)

name it

![2](../img/ed/ed2.png)

use the *sketch*

![3](../img/ed/ed3.png)

to seach something in libraries

![4](../img/ed/ed4.png)

in fab libs, search up the components u want

![5](../img/ed/ed5.png)

search symbol

![6](../img/ed/ed6.png)

place symbol

![7](../img/ed/ed7.png)

for the atmega 32 put the whole name

![8](../img/ed/ed8.png)

after placing components, annotate schematic symbols

![10](../img/ed/ed10.png)

click annotate

![11](../img/ed/ed11.png)

it will load ur footprints

![12](../img/ed/ed12.png)

manage footprint libraries

![13](../img/ed/ed13.png)

use the file *fab.mot*

![14](../img/ed/ed14.png)

click on generate netlist

![15](../img/ed/ed15.png)

click on *enable acceleration*

![16](../img/ed/ed16.png)

click on the *pcb thing* to get the red part

![17](../img/ed/ed17.png)

read current netlist to save ur changes or apply ur changes and also direct to the file where u saved ur project

![18](../img/ed/ed18.png)

read current netlist

![19](../img/ed/ed19.png)

desing rules

![20](../img/ed/ed20.png)

arrange them in order
and plot ur file

![21](../img/ed/ed21.png)

direct to the file again and put the following options as shown

![22](../img/ed/ed22.png)


***FLATCAM***

install if needed:   http://flatcam.org/download

![23](../img/ed/ed23.png)

open the file and pur in the following params:

![24](../img/ed/ed25.png)

![25](../img/ed/ed26.png)

![26](../img/ed/ed27.png)

![27](../img/ed/ed28.png)

![28](../img/ed/ed29.png)

![29](../img/ed/ed30.png)

