**EMBEDDED PROGRAMMING**

 open aduino and go to file -> new -> and name ur project
 
![1](../img/1.png)

go to tools-> port> com8-. and select the aduino uno

![2](../img/2.png)

go to tools->board (whichever board u have chosen)-> and select the exact board

![3](../img/3.png)

*blink*

to blink, go to examples->basics->blink

![4](../img/4.png)

put the number where i've put the light as shown

![5](../img/5.png)

*spaceship interface*

for spaceship interface, put lights as input and output

![6](../img/6.png)

determine high and low functions and the delay between them *still spaceship interface*

![7](../img/7.png)

contuation for spaceship interface code...

![8](../img/8.png)

go to tools ->serial monitor to check the light value

![9](../img/9.png)

result of the serial monitor

![10](../img/10.png)

*procedural programming*

for running lights code:

![11](../img/11.png)

continuation code

![12](../img/12.png)

continuation...

![13](../img/13.png)

*serial communication*

 determine min and max values and 

![14](../img/14.png)

continuation procedure...

![15](../img/15.png)

*light dependent resistor*

'''

int lightInt= 0;
//int analogPin= A0;
int minLight=800;
int maxLight=1023;
int lightPc=0;                          
//int sensorValue= 0;

void setup() {                          
       Serial.println("Party");
       Serial.begin(9600);  
       pinMode(2,OUTPUT);
       pinMode(3,OUTPUT);
       pinMode(4,OUTPUT); 
}                                       
                                        
void LedsOff(){
    digitalWrite(2,LOW);
    digitalWrite(3,0);
    digitalWrite(4,0);
}

void LedsRunning(){
    LedsOff();
    digitalWrite(2,1);
    delay(250);
    LedsOff();
    digitalWrite(3,1);
    delay(250);
    LedsOff();
    digitalWrite(4,1);
    delay(250);                                   
}

void loop() {
    lightInt = analogRead(A0); 
    lightPc = (lightInt-minLight)*100L/(maxLight-minLight); // read the input on analog pin 0
    Serial.println(lightPc); 
    LedsOff();
      if(lightPc >=30){digitalWrite(2,1);};
      if(lightPc >=60){digitalWrite(3,1);};
      if(lightPc >=90){digitalWrite(4,1);};
      if(lightPc >=95){LedsRunning();};                       
      delay(1000);
}





'''

*H-bridge*

![16](../img/hbridge.png)

'''

const int pwm = 3 ; //initializing pin 2 as pwm
const int in_1 = 8 ;
const int in_2 = 9 ;
//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {
   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output
   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output
   pinMode(in_2,OUTPUT) ;
}

'''

*dc motor control*

![17](../img/dcmotor.png)

'''
void loop() {
   //For Clock wise motion , in_1 = High , in_2 = Low
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,LOW) ;
   analogWrite(pwm,255) ;
   /* setting pwm of the motor to 255 we can change the speed of rotation
   by changing pwm input but we are only using arduino so we are using highest
   value to driver the motor */
   //Clockwise for 3 secs
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH
   digitalWrite(in_1,LOW) ;
   digitalWrite(in_2,HIGH) ;
   delay(3000) ;
   //For brake
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,HIGH) ;
   delay(1000) ;
}

'''

*mapping function*

'''

//Declare ur variables
 const int pwm =3;
 const int in_1=8;
 const int in_2=9;

void setup()
{
  pinMode(pwm, OUTPUT);
  pinMode(in_1, OUTPUT);
  pinMode(in_2, OUTPUT);
  pinMode(3, OUTPUT);
  Serial.begin(9600);
}

void loop(){
  int duty = map(analogRead(A0),0,1023,-255,255);
  Serial.println(duty);
  analogWrite(pwm,abs(duty));
  if (duty>0){
// turn CW
	digitalWrite(in_1,LOW);
	digitalWrite(in_2,HIGH);
  }
    if (duty<0){
// turn CCW
	digitalWrite(in_1,HIGH);
	digitalWrite(in_2,LOW);
  }
      if (duty==0){
// turn CCW
	digitalWrite(in_1,HIGH);
	digitalWrite(in_2,HIGH);
  }   
}
 
 '''
 
 *Servo motor control*
 
![18](../img/servo.png)

'''

#include <Servo.h>

Servo myServo;

int const potPin = A0 ;
int potVal;  
int angle;

void setup()
{
 myServo.attach(9);
 Serial.begin(9600);
}
  
void loop() {
 potVal = analogRead(potPin);
 Serial.print("potVal:");
 Serial.print(potVal);
 angle = map(potVal, 0, 1023, 0, 179);
 Serial.print(", angle: ");
 Serial.println(angle); 
 myServo.write(angle);
 delay(15);
}

'''



