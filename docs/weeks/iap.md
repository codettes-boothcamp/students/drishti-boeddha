***RASPBERRY PI***

**plug the raspberry pi in the pc**

open command prompt in the search section

![1](../img/iap1.png)

thats the ip adress u have (*everyone has his own*) *to check if ur raspberry pi responds, open this again and type ping (the code)*

![2](../img/iap2.png)

open *win32disk imager*

![3](../img/iap3.png)

add the raspbiam jessie image

![4](../img/iap4.png)

check the device ur using and click on write then 

![5](../img/iap6.png)

2 warnings will pop up but click on *no*

now u can see the *boot file*

![6](../img/iap9.png)

go to *boot file* and open the *mndline.txt* and edit it with notepad

![7](../img/iap10.png)

after the whole command, click on *spacebar* then *ip-and whatever ur code was* (*to change the command*)

after that, *right click anywhere and add a new task document and call it* **ssh**

now we r gonna do a hack-> *so we r gonna remote access to the raspberry pie to fool it to think that we did inserted stuff but actually we didnt*
for that go to wifi

then go to *command prompt and put ping the ur command*

![8](../img/iap15.png)

then the username is : **pi**
the password is : **raspberry** (*the password will not show up*)

![9](../img/iap16.png)

open *putty*
use the arrows to move
*enable camera*
*advanced options* **enable all ***except update*** **
*click **no** when asking to reboot*

**install nodemon through this link in putty**

$ curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -

then copy this code in index.js file

"
  // index.js

/**
 * Required External Modules
 */

const express = require("express");
const path = require("path");

/**
 * App Variables
 */

const app = express();
const port = process.env.PORT || "8000";

/**
 *  App Configuration
 */

/*app.get("/", (req, res) => {
  res.status(200).send("WHATABYTE: Food For Devs");*/

app.use(express.static(path.join(__dirname, "public")));

/**
 * Routes Definitions
 */

app.get("/", (req, res) => {
  res.status(200).send("WHATABYTE: Food For Devs");
});

/**
 * Server Activation
 */

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
}); 
"

then go to putty and put *npm init -y*

![10](../img/pynode/node1.png)

to install the nodemon put this command in putty *npm i -D nodemon*

![11](../img/pynode/node2.png)

to add the frame for node, add this command *npm i express*
it then gives u a port *8000*
to run it, add *npm run dev*
so go on chrome, type ur ip adress from ur raspberry and att this *:5000*
this is ur web page

![12](../img/pynode/node3.png)

adjust ur web page as u want with index.js by looking up on google how to run javascrpit


**to install pyhton, copy this code in *index.py* **

"
 from flask import Flask, render_template

app = Flask(__name__)

@app.route('/')
def index():
    return render_template('index.html')
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0') 
"
    
then go to putty and do *python index.py*
    
![13](../img/pynode/py1.png)

to run it, say *sudo pip install flask*

u will get the port *5000* and add ur raspberry pi's number with *:5000*

![14](../img/pynode/py2.png)

**serial communication**

build the circuit and open *arduino uno* and select the following steps:

![15](../img/pynode/ser1.png)

ofcourse' check if ur led blinks with just normal blink
create a file with any name.py

go to *whatever name u gave.py* and select the following code:

"
"import serial
import time
# Define the serial port and baud rate.
# Ensure the 'COM#' corresponds to what was seen in the Windows Device Manager
ser = serial.Serial('/dev/ttyAMA0 ', 9600)
def led_on_off():
    user_input = input("\n Type on / off / quit : ")
    if user_input =="on":
        print("LED is on...")
        time.sleep(0.1) 
        ser.write(b'H') 
        led_on_off()
    elif user_input =="off":
        print("LED is off...")
        time.sleep(0.1)
        ser.write(b'L')
        led_on_off()
    elif user_input =="quit" or user_input == "q":
        print("Program Exiting")
        time.sleep(0.1)
        ser.write(b'L')
        ser.close()
    else:
        print("Invalid input. Type on / off / quit.")
        led_on_off()

time.sleep(2) # wait for the serial connection to initialize

led_on_off() 
"

save this and test it on putty

**JAVASCRIPT**

javascript allows u to make ur web structures(base for the web)

run ur nodejs with nmp run dev and paste the next code in *index.html* to form the base of the web:

"
 <!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Title</title>
    <link href="css/style.css" rel="stylesheet" />
    <meta name = "viewport" content = "width=device-width, initial-scale=1.0">
</head>

<body>
    <header class = "mainHeader">
    	<img src="img/logo.png">
	<nav>
	      <ul>
	            <li><a href="#" class="active">Home</a></li>
	            <li><a href="#">About</a></li>
	            <li><a href="#">Portfolio</a></li>
	            <li><a href="#">Gallery</a></li>
	            <li><a href="#">Contact</a></li>
	  	</ul>
        </nav>
    </header>
    
    <div class="mainContent">
	    <div class="content">
    		<article class="articleContent">
	            <header>
	                <h2>First Article #1</h2>
	            </header>
	            
	            <footer>
	            	<p class="post-info">Written by Super Woman</p>	            	
	            </footer>
	            <content>
	            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        		</content>
    		</article> 
    		
    		<article class="articleContent">
	            <header>
	                <h2>2nd Article #2</h2>
	            </header>
	            
	            <footer>
	            	<p class="post-info">Written by Super Woman</p>	            	
	            </footer>

	            <content>
	            	<p>This is the actual article content ... in this case a teaser for some other page ... read more</p>
        		</content>
    		</article> 
    		
	   </div>
	</div>
	
	<aside class="top-sidebar">
		<article>
			<h2>Top sidebar</h2>
			<p>Lorum ipsum dolorum on top</p>
		</article>
	</aside>
	
	<aside class="middle-sidebar">
		<article>
			<h2>Middle sidebar</h2>
			<p>Lorum ipsum doloru in the middle</p>
		</article>
	</aside>
	
	<aside class="bottom-sidebar">
		<article>
			<h2>Bottom sidebar</h2>
			<p>Lorum ipsum dolorum at the bottom</p>
		</article>
	</aside>
	
	<footer class="mainFooter">
		<p>Copyright &copy; <a href="#" title = " MyDesign">mywebsite.com</a></p>
	</footer>
		
</body>

</html>
"

after the code, go to ur web page and *right click* and *inspect* to make certain changes like background color etc.

![16](../img/inspect.png)

replace the code on the right with the following code in *utils.js* and to make the chart visible, add it to *index.html*

"
/* 
	Stylesheet for:
	HTML5-CSS3 Responsive page
*/

/* body default styling */

body {
    /* border: 5px solid #f1073d; */
    background-image: url(img/bg.png);
    background-color: #dc092d;
    font-size: 87.5%;
    /* base font 14px */
    font-family: Arial, 'Lucinda Sans Unicode';
    line-height: 1.5;
    text-align: left;
    margin: 0 auto;
    width: 70%;
    clear: both;
}


/* style the link tags */
a {
    text-decoration: none;
}

a:link a:visited {
    color: #8B008B;
}

a:hover,
a:active {
    background-color: #8B008B;
    color: #FFF;
}


/* define mainHeader image and navigation */
.mainHeader img {
    width: 30%;
    height: auto;
    margin: 2% 0;
}

.mainHeader nav {
    background-color: #666;
    height: 40px;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainHeader nav ul {
    list-style: none;
    margin: 0 auto;
}

.mainHeader nav ul li {
    float: left;
    display: inline;
}

.mainHeader nav a:link,
mainHeader nav a:visited {
    color: #FFF;
    display: inline-block;
    padding: 10px 25px;
    height: 20px;
}

.mainHeader nav a:hover,
.mainHeader nav a:active,
.mainHeader nav .active a:link,
.mainHeader nav a:active a:visited {
    background-color: #8B008B;
    /* Color purple */
    text-shadow: none;
}

.mainHeader nav ul li a {
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}


/* style the contect sections */

.mainContent {
    line-height: 20px;
    overflow: none;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.content {
    width: 70%;
    float: left;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.articleContent {
    background-color: #FFF;
    padding: 3% 5%;
    margin-top: 2%;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.post-info {
    font-style: italic;
    color: #999;
    font-size: 85%;
}

.top-sidebar {
    width: 18%;
    margin: 1.5% 0 1% 2%;
    padding: 3% 5%;
    float: left;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.middle-sidebar {
    width: 18%;
    margin: 1.5% 0 1% 2%;
    padding: 3% 5%;
    float: left;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.bottom-sidebar {
    width: 18%;
    margin: 1.5% 0 1% 2%;
    padding: 3% 5%;
    float: left;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainFooter {
    width: 100%;
    height: 40px;
    float: left;
    border-color: #666;
    background-color: #666;
    margin: 2% 0;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainFooter p {
    width: 92%;
    margin: 10px auto;
    color: #FFF;
    text-align: center;
}
" 

**for charts**

add a text document called *utils.js*
download a chart and copy the following chart code in utils.js

"
'use strict';

window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};

(function(global) {
	var MONTHS = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
		'#4dc9f6',
		'#f67019',
		'#f53794',
		'#537bc4',
		'#acc236',
		'#166a8f',
		'#00a950',
		'#58595b',
		'#8549ba'
	];

	var Samples = global.Samples || (global.Samples = {});
	var Color = global.Color;

	Samples.utils = {
		// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
		srand: function(seed) {
			this._seed = seed;
		},

		rand: function(min, max) {
			var seed = this._seed;
			min = min === undefined ? 0 : min;
			max = max === undefined ? 1 : max;
			this._seed = (seed * 9301 + 49297) % 233280;
			return min + (this._seed / 233280) * (max - min);
		},

		numbers: function(config) {
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 1;
			var from = cfg.from || [];
			var count = cfg.count || 8;
			var decimals = cfg.decimals || 8;
			var continuity = cfg.continuity || 1;
			var dfactor = Math.pow(10, decimals) || 0;
			var data = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = (from[i] || 0) + this.rand(min, max);
				if (this.rand() <= continuity) {
					data.push(Math.round(dfactor * value) / dfactor);
				} else {
					data.push(null);
				}
			}

			return data;
		},

		labels: function(config) {
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 100;
			var count = cfg.count || 8;
			var step = (max - min) / count;
			var decimals = cfg.decimals || 8;
			var dfactor = Math.pow(10, decimals) || 0;
			var prefix = cfg.prefix || '';
			var values = [];
			var i;

			for (i = min; i < max; i += step) {
				values.push(prefix + Math.round(dfactor * i) / dfactor);
			}

			return values;
		},

		months: function(config) {
			var cfg = config || {};
			var count = cfg.count || 12;
			var section = cfg.section;
			var values = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = MONTHS[Math.ceil(i) % 12];
				values.push(value.substring(0, section));
			}

			return values;
		},

		color: function(index) {
			return COLORS[index % COLORS.length];
		},

		transparentize: function(color, opacity) {
			var alpha = opacity === undefined ? 0.5 : 1 - opacity;
			return Color(color).alpha(alpha).rgbString();
		}
	};

	// DEPRECATED
	window.randomScalingFactor = function() {
		return Math.round(Samples.utils.rand(-100, 100));
	};

	// INITIALIZATION

	Samples.utils.srand(Date.now());

	// Google Analytics
	/* eslint-disable */
	if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-28909194-3', 'auto');
		ga('send', 'pageview');
	}
	/* eslint-enable */
}(this));
"

![17](../img/iap30.png)

we can replace the chart line with changing the *line into graph*

![18](../img/iap32.png)

**adding a button*

add the following code in *button.css*

"
.onoffswitch {
    position: relative; width: 55px;
    -webkit-user-select:none; -moz-user-select:none; -ms-user-select: none;
}
.onoffswitch-checkbox {
    display: none;
}
.onoffswitch-label {
    display: block; overflow: hidden; cursor: pointer;
    height: 20px; padding: 0; line-height: 20px;
    border: 0px solid #FFFFFF; border-radius: 30px;
    background-color: #00AAFF;
}
.onoffswitch-label:before {
    content: "";
    display: block; width: 30px; margin: -5px;
    background: #620280;
    position: absolute; top: 0; bottom: 0;
    right: 31px;
    border-radius: 30px;
    box-shadow: 0 6px 12px 0px #757575;
}
.onoffswitch-checkbox:checked + .onoffswitch-label {
    background-color: #0985EB;
}
.onoffswitch-checkbox:checked + .onoffswitch-label, .onoffswitch-checkbox:checked + .onoffswitch-label:before {
   border-color: #0985EB;
}
.onoffswitch-checkbox:checked + .onoffswitch-label .onoffswitch-inner {
    margin-left: 0;
}
.onoffswitch-checkbox:checked + .onoffswitch-label:before {
    right: 0px; 
    background-color: #FF0099; 
    box-shadow: 3px 6px 18px 0px rgba(0, 0, 0, 0.2);
}
"

the web must look like this

"
 <!DOCTYPE html>
<html>

<head>
<html>

<head>
    <script src="js/lib/Chart.js/Chart.js"></script>
    <script src="js/utils.js"></script>
    
    <meta charset="utf-8" />
    <title>Title</title>
    <link href="css/style.css" rel="stylesheet" />
	<link href="css/button.css" rel="stylesheet" />
    <meta name = "viewport" content = "width=device-width, initial-scale=1.0">
   <script src="js/lib/Chart.js/Chart.js"></script>
    <script src="js/utils.js"></script>


<body>
    <header class = "mainHeader">
    	<img src="img/logo.png">
	<nav>
	      <ul>
	            <li><a href="#" class="active">Home</a></li>
	            <li><a href="#">About</a></li>
	            <li><a href="#">Portfolio</a></li>
	            <li><a href="#">Gallery</a></li>
	            <li><a href="#">Contact</a></li>
	  	</ul>
        </nav>
    </header>
    
    <div class="mainContent">
	    <div class="content">
    		<article class="articleContent">
	            <header>
	                <h2>First Article #1</h2>
	            </header>
	            
	            <footer>
	            	<p class="post-info">Written by Super Woman</p>	            	
	            </footer>
	            <content>
	            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        		</content>
    		</article> 
    		
    		<article class="articleContent">
	            <header>
	                <h2>2nd Article #2</h2>
	            </header>
	            
	            <footer>
	            	<p class="post-info">Written by Super Woman</p>	            	
	            </footer>

	            <content>
	            	<div style="width:100%; height:100%">
				<canvas id="canvas"></canvas>
				<button id="randomizeData">Randomize Data</button>
			    <button id="addDataset">Add Dataset</button>
			    <button id="removeDataset">Remove Dataset</button>
			    <button id="addData">Add Data</button>
			    <button id="removeData">Remove Data</button>
				<script>
		var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
		var config = {
			type: 'bar',
			data: {
				labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
				datasets: [{
					label: 'My First dataset',
					backgroundColor: window.chartColors.red,
					borderColor: window.chartColors.red,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
					fill: false,
				}, {
					label: 'My Second dataset',
					fill: false,
					backgroundColor: window.chartColors.blue,
					borderColor: window.chartColors.blue,
					data: [
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor(),
						randomScalingFactor()
					],
				}]
			},
			options: {
				responsive: true,
				title: {
					display: true,
					text: 'Chart.js Line Chart'
				},
				tooltips: {
					mode: 'index',
					intersect: false,
				},
				hover: {
					mode: 'nearest',
					intersect: true
				},
				scales: {
					xAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Month'
						}
					}],
					yAxes: [{
						display: true,
						scaleLabel: {
							display: true,
							labelString: 'Value'
						}
					}]
				}
			}
		};

		window.onload = function() {
			var ctx = document.getElementById('canvas').getContext('2d');
			window.myLine = new Chart(ctx, config);
		};

		document.getElementById('randomizeData').addEventListener('click', function() {
			config.data.datasets.forEach(function(dataset) {
				dataset.data = dataset.data.map(function() {
					return randomScalingFactor();
				});

			});

			window.myLine.update();
		});

		var colorNames = Object.keys(window.chartColors);
		document.getElementById('addDataset').addEventListener('click', function() {
			var colorName = colorNames[config.data.datasets.length % colorNames.length];
			var newColor = window.chartColors[colorName];
			var newDataset = {
				label: 'Dataset ' + config.data.datasets.length,
				backgroundColor: newColor,
				borderColor: newColor,
				data: [],
				fill: false
			};

			for (var index = 0; index < config.data.labels.length; ++index) {
				newDataset.data.push(randomScalingFactor());
			}

			config.data.datasets.push(newDataset);
			window.myLine.update();
		});

		document.getElementById('addData').addEventListener('click', function() {
			if (config.data.datasets.length > 0) {
				var month = MONTHS[config.data.labels.length % MONTHS.length];
				config.data.labels.push(month);

				config.data.datasets.forEach(function(dataset) {
					dataset.data.push(randomScalingFactor());
				});

				window.myLine.update();
			}
		});

		document.getElementById('removeDataset').addEventListener('click', function() {
			config.data.datasets.splice(0, 1);
			window.myLine.update();
		});

		document.getElementById('removeData').addEventListener('click', function() {
			config.data.labels.splice(-1, 1); // remove the label first

			config.data.datasets.forEach(function(dataset) {
				dataset.data.pop();
			});

			window.myLine.update();
		});
	</script>
		</div>

        		</content>
    		</article> 
    		
	   </div>
	</div>
	
	<aside class="top-sidebar">
		<article>
			<h2>Top sidebar</h2>
			<p>Lorum ipsum dolorum on top</p>
			<div class="onoffswitch">
              <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch" checked>
              <label class="onoffswitch-label" for="myonoffswitch"></label>
		</article>
	</aside>
	
	<aside class="middle-sidebar">
		<article>
			<h2>Middle sidebar</h2>
			<p>Lorum ipsum doloru in the middle</p>
		</article>
	</aside>
	
	<aside class="bottom-sidebar">
		<article>
			<h2>Bottom sidebar</h2>
			<p>Lorum ipsum dolorum at the bottom</p>
		</article>
	</aside>
	
	<footer class="mainFooter">
		<p>Copyright &copy; <a href="#" title = " MyDesign">mywebsite.com</a></p>
	</footer>
		
</body>

</html>
"

***SCADA***



build a led circuit and download scada
make a new project file in nodejs -> *nodejs- projects- project2/3, depending on ur content*
then extract scada 

![30](../img/scada1.png)

open *winscape32* and copy paste scada to projects
open scada in index.html and change the port to ur port
open scada in putty-> project2/3-> *node app.js*
*socket io allows to switch leds*
*express makes instance of nodejs*
*pigpio talks to pin for general output and input*
port is given(4000), so go to web browser with ur port :4000

[31](../img/scada2.png)

***LCD***

build the circuit->open arduino app->and select the following functions:

![32](../img/lds1.png)

enter the code for the circuit

![33](../img/lds2.png)

this is the function for whenever u add a sensor













