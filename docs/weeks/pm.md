## Project Managment


**day 1:**

 make a gitlab and github account
 make a local github

 clone the online gitlab file to the local github account using url

![clone](../img/pm/1.png)

 clone using githubdesktop

![notepad](../img/pm/2.png)

 Local Filesystem

![filesystem](../img/pm/3.png)
 
 edit with notepad ++
 
![filesystem](../img/pm/4.png)
 
 changes made in githubdesktop and commit text
 
 ![github](../img/pm/5.png)
 
 git push
 
 ![commit-text](../img/pm/6.png)
 
 changes made
 
 ![push](../img/pm/7.png)
 


